FactoryBot.define do
  factory :app_setting do
    sequence(:key) { |n| "setting_#{n}" }
    value { "value" }
  end
end
