require "rails_helper"

describe "/payment-amount" do
  describe "GET" do
    let(:params) do
      { asking_price: "550000",
        down_payment: "50000",
        payment_schedule: "monthly",
        amortization_period: "25" }
    end
    let(:valid_quote) { instance_double(Mortgage::PaymentQuote, valid?: true, payment_amount: BigDecimal("1000.0")) }
    let(:invalid_quote) { instance_double(Mortgage::PaymentQuote, valid?: false) }
    let(:quote_errors) { instance_double(ActiveModel::Errors, full_messages: ["Bad input"]) }

    it "returns payment amount as calculated by PaymentQuote" do
      allow(Mortgage::PaymentQuote).to receive(:new).and_return(valid_quote)

      get "/payment-amount", params: params

      expect(response.body).to eq({ payment_amount: "1000.0" }.to_json)
    end

    it "returns validation errors when params are invalid" do
      allow(Mortgage::PaymentQuote).to receive(:new).and_return(invalid_quote)
      allow(invalid_quote).to receive(:errors).and_return(quote_errors)

      get "/payment-amount", params: params

      expect(response.body).to eq({ errors: ["Bad input"] }.to_json)
    end
  end
end
