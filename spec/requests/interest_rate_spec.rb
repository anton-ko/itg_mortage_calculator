require "rails_helper"

describe "/interest-rate" do
  let(:json_headers) { { "ACCEPT" => "application/json", "CONTENT_TYPE" => "application/json" } }

  describe "PATCH" do
    let(:new_rate) { 0.05 }
    let!(:old_rate) { create(:interest_rate).rate }

    it "sets current interest rate" do
      patch "/interest-rate", params: { rate: new_rate.to_s }.to_json, headers: json_headers

      expect(response).to have_http_status(:ok)
      expect(response.content_type).to eq("application/json; charset=utf-8")

      parsed_response = JSON.parse(response.body)
      expect(parsed_response).to eq("previous_rate" => old_rate.to_s, "current_value" => new_rate.to_s)
    end

    it "returns validation errors" do
      patch "/interest-rate", params: { rate: 0 }.to_json, headers: json_headers

      expect(response).to have_http_status(:bad_request)
      expect(response.content_type).to eq("application/json; charset=utf-8")

      parsed_response = JSON.parse(response.body)
      expect(parsed_response).to eq("errors" => ["Rate must be greater than 0"])
    end
  end
end
