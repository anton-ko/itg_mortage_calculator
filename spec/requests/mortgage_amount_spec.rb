require "rails_helper"

describe "/mortgage-amount" do
  describe "GET" do
    let(:params) { { payment_amount: "1000", payment_schedule: "monthly", amortization_period: "25" } }
    let(:valid_quote) { instance_double(Mortgage::MaxMortgageQuote, valid?: true, principal: BigDecimal("100000.00")) }
    let(:invalid_quote) { instance_double(Mortgage::MaxMortgageQuote, valid?: false) }
    let(:quote_errors) { instance_double(ActiveModel::Errors, full_messages: ["Bad input"]) }

    it "returns max mortgage amount as calculated by MaxMortgageQuote" do
      allow(Mortgage::MaxMortgageQuote).to receive(:new).and_return(valid_quote)

      get "/mortgage-amount", params: params

      expect(response.body).to eq({ maximum_mortgage_amount: "100000.0" }.to_json)
    end

    it "returns validation errors when params are invalid" do
      allow(Mortgage::MaxMortgageQuote).to receive(:new).and_return(invalid_quote)
      allow(invalid_quote).to receive(:errors).and_return(quote_errors)

      get "/mortgage-amount", params: params

      expect(response.body).to eq({ errors: ["Bad input"] }.to_json)
    end
  end
end
