require "rails_helper"

RSpec.describe AppSetting, type: :model do
  describe "Validations" do
    it { is_expected.to validate_uniqueness_of :key }
  end
end
