require "rails_helper"

RSpec.describe Mortgage::MinDownPayment do
  describe ".calculate" do
    it "calculates correctly" do
      expect(described_class.calculate(100_000)).to eq(5_000)
      expect(described_class.calculate(750_000)).to eq(50_000)
      expect(described_class.calculate(1_000_000)).to eq(75_000)
    end

    it "handles strings" do
      expect(described_class.calculate("100000")).to eq(5_000)
    end
  end
end
