require "rails_helper"

RSpec.describe Mortgage::MaxMortgageQuote, type: :model do
  describe "#principal" do
    before do
      allow(InterestRate).to receive(:current_value).and_return(BigDecimal("0.03"))
    end

    it "calculates correct amount" do
      quote = Mortgage::MaxMortgageQuote.new(payment_amount: "1000",
                                             payment_schedule: Mortgage::PaymentSchedule::MONTHLY,
                                             amortization_period: Mortgage::AmortizationPeriod::MAX_YEARS.to_s)

      expect(quote.principal.to_s).to eq("210876.45")
    end

    it "raises on invalid input" do
      expect { Mortgage::MaxMortgageQuote.new(payment_amount: -5).principal }.to raise_error(RuntimeError)
    end
  end

  describe "validations" do
    it { is_expected.to validate_numericality_of(:payment_amount).is_greater_than(0) }

    example "payment_schedule" do
      expect(described_class.new).to(validate_inclusion_of(:payment_schedule)
                                       .in_array(Mortgage::PaymentSchedule::FREQUENCIES)
                                       .with_message("can only be weekly, biweekly or monthly"))
    end

    example "amortization_period" do
      expect(described_class.new).to(validate_numericality_of(:amortization_period)
                                       .is_greater_than_or_equal_to(Mortgage::AmortizationPeriod::MIN_YEARS)
                                       .is_less_than_or_equal_to(Mortgage::AmortizationPeriod::MAX_YEARS)
                                       .only_integer)
    end
  end
end
