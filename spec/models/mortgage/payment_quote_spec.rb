require "rails_helper"

RSpec.describe Mortgage::PaymentQuote, type: :model do
  describe "#payment_amount" do
    before do
      allow(InterestRate).to receive(:current_value).and_return(BigDecimal("0.05"))
    end

    it "calculates insured monthly mortgage correctly" do
      quote = described_class.new(asking_price: 100_000,
                                  down_payment: 10_000,
                                  amortization_period: 10,
                                  payment_schedule: Mortgage::PaymentSchedule::MONTHLY)
      expect(quote.payment_amount.to_s).to eq("977.5")
    end

    it "calculates insured bi-weekly mortgage correctly" do
      quote = described_class.new(asking_price: 700_000,
                                  down_payment: 50_000,
                                  amortization_period: 25,
                                  payment_schedule: Mortgage::PaymentSchedule::BIWEEKLY)
      expect(quote.payment_amount.to_s).to eq("1808.0")
    end

    it "calculates no-insurance mortgage correctly" do
      quote = described_class.new(asking_price: 1_250_000,
                                  down_payment: 250_000,
                                  amortization_period: 25,
                                  payment_schedule: Mortgage::PaymentSchedule::MONTHLY)
      expect(quote.payment_amount.to_s).to eq("5845.9")
    end

    it "handles string arguments" do
      quote = described_class.new(asking_price: "100000",
                                  down_payment: "10000",
                                  amortization_period: "10",
                                  payment_schedule: Mortgage::PaymentSchedule::MONTHLY)
      expect(quote.payment_amount.to_s).to eq("977.5")
    end
  end

  describe "validations" do
    it { is_expected.to validate_numericality_of(:asking_price).is_greater_than(0) }

    example "payment_schedule" do
      expect(described_class.new).to(validate_inclusion_of(:payment_schedule)
                                       .in_array(Mortgage::PaymentSchedule::FREQUENCIES)
                                       .with_message("can only be weekly, biweekly or monthly"))
    end

    example "amortization_period" do
      expect(described_class.new).to(validate_numericality_of(:amortization_period)
                                       .is_greater_than_or_equal_to(Mortgage::AmortizationPeriod::MIN_YEARS)
                                       .is_less_than_or_equal_to(Mortgage::AmortizationPeriod::MAX_YEARS)
                                       .only_integer)
    end

    describe "down_payment" do
      it { is_expected.to validate_numericality_of(:down_payment) }

      example "down_payment must be above min_down_payment" do
        quote = described_class.new(asking_price: 100_000, down_payment: 1_000) # min_down_payment = 5000
        quote.validate
        expect(quote.errors[:down_payment]).to include("must be greater than or equal to #{quote.min_down_payment}")
      end

      example "down_payment must be below asking_price" do
        quote = described_class.new(asking_price: 10_000, down_payment: 10_000)
        quote.validate
        expect(quote.errors[:down_payment]).to include("must be less than 10000")
      end
    end
  end
end
