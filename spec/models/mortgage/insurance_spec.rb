require "rails_helper"

RSpec.describe Mortgage::Insurance, type: :model do
  describe "#price" do
    it "is calculated correctly" do
      expect(described_class.new(100_000, 5).cost.to_s).to eq("3150.0")    # 100000 * 3.15%
      expect(described_class.new(100_000, 9.99).cost.to_s).to eq("3150.0") # 100000 * 3.15%
      expect(described_class.new(100_000, 10).cost.to_s).to eq("2400.0")   # 100000 * 2.4%
      expect(described_class.new(100_000, 15).cost.to_s).to eq("1800.0")   # 100000 * 1.8%
    end

    it "is zero for mortgages with large downpayment" do
      expect(described_class.new(100_000, 20).cost.to_s).to eq("0")
    end

    it "is zero for mortgages above the limit" do
      expect(described_class.new(1_000_001, 5).cost.to_s).to eq("0")
    end
  end
end
