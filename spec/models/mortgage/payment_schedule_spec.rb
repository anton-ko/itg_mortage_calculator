require "rails_helper"

RSpec.describe Mortgage::PaymentSchedule do
  describe ".payments_over_period" do
    example "returns the number of payments over the amortization_period" do
      expect(Mortgage::PaymentSchedule.payments_over_period(Mortgage::PaymentSchedule::MONTHLY, 1)).to eq(12)
      expect(Mortgage::PaymentSchedule.payments_over_period(Mortgage::PaymentSchedule::BIWEEKLY, 1)).to eq(26)
      expect(Mortgage::PaymentSchedule.payments_over_period(Mortgage::PaymentSchedule::WEEKLY, 1)).to eq(52)
      expect(Mortgage::PaymentSchedule.payments_over_period(Mortgage::PaymentSchedule::MONTHLY, 10)).to eq(120)
      expect(Mortgage::PaymentSchedule.payments_over_period(Mortgage::PaymentSchedule::BIWEEKLY, 5)).to eq(130)
    end
  end
end
