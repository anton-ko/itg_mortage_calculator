require "rails_helper"

RSpec.describe InterestRate, type: :model do
  describe ".current_value" do
    subject(:current_value) { InterestRate.current_value }

    it "is nil when InterestRate doesn't exists" do
      expect(current_value).to eq(nil)
    end

    it "returns value of the current rate" do
      interest_rate = create(:interest_rate)
      expect(current_value).to eq(interest_rate.rate)
    end
  end

  describe "#rate" do
    it "is casted to decimal" do
      record = InterestRate.new(rate: "0.05")
      expect(record.rate).to be_a(BigDecimal)
      expect(record.rate).to eq(0.05)
    end
  end

  describe "validations" do
    describe "value is between 0 and 1" do
      it { is_expected.to validate_numericality_of(:rate).is_greater_than(0).is_less_than(1) }
    end
  end
end
