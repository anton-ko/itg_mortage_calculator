class CreateAppSettings < ActiveRecord::Migration[6.1]
  def change
    create_table(:app_settings) do |t|
      t.string(:key, index: { unique: true })
      t.text(:value)

      t.timestamps
    end
  end
end
