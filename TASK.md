When completed please send us a link to the source control system you used to host the code for us to review it. Feel free to ask us for any clarifications on the requirements.

## Requirements

Create a mortgage calculator API using any language/tech stack.

### GET /payment-amount
Get the recurring payment amount of a mortgage

Params: (query string)
* Asking Price
* Down Payment*
* Payment schedule***
* Amortization Period**

Return:
        Payment amount per scheduled payment in JSON format


### GET /mortgage-amount
Get the maximum mortgage amount (principal)

Params: (query string)
* payment amount
* Payment schedule***
* Amortization Period**

Return:
Maximum Mortgage that can be taken out in JSON format

### PATCH /interest-rate
Change the interest rate used by the application

Params: (in request body as JSON)
* Interest Rate

Return:
message indicating the old and new interest rate in JSON format


* &ast; Must be at least 5% of first $500k plus 10% of any amount above $500k (So $50k on a $750k mortgage)
* &ast;&ast; Min 5 years, max 25 years
* &ast;&ast;&ast; Weekly, biweekly, monthly

Mortgage interest rate 2.5% per year

Mortgage insurance is required on all mortgages with less than 20% down.  Insurance must be calculated and added to the mortgage principal. Mortgage insurance is not available for mortgages > $1 million.

Mortgage insurance rates are as follows:
```
Down payment     Insurance Cost
5-9.99%        		3.15%
10-14.99%    		2.4%
15%-19.99%  		1.8%
20%+         		N/A
```
See this link for information our calculating mortgage payments: https://www.wikihow.com/Calculate-Mortgage-Payments
