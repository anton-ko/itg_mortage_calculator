class MaxMortgageAmountController < ApplicationController
  def show
    quote = Mortgage::MaxMortgageQuote.new(max_mortgage_params)

    if quote.valid?
      render json: { "maximum_mortgage_amount" => quote.principal.truncate(2).to_s("F") }
    else
      render json: { errors: quote.errors.full_messages }, status: :bad_request
    end
  end

  private

  def max_mortgage_params
    params.permit(:payment_amount, :payment_schedule, :amortization_period)
  end
end
