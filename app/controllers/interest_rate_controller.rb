class InterestRateController < ApplicationController
  def update
    model = InterestRate.current
    rate = model.rate
    if model.update(rate: params[:rate])
      response = { previous_rate: rate.to_s,
                   current_value: model.rate.to_s }
      render json: response
    else
      render json: { errors: model.errors.full_messages },
             status: :bad_request
    end
  end
end
