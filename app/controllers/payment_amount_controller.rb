class PaymentAmountController < ApplicationController
  def show
    quote = Mortgage::PaymentQuote.new(payment_amount_params)
    if quote.valid?
      render json: { "payment_amount" => quote.payment_amount.truncate(2).to_s("F") }
    else
      render json: { errors: quote.errors.full_messages }, status: :bad_request
    end
  end

  private

  def payment_amount_params
    params.permit(:asking_price, :down_payment, :payment_schedule, :amortization_period)
  end
end
