# frozen-string-literal: true

module Mortgage
  module PaymentSchedule
    FREQUENCIES = [
      WEEKLY = "weekly",
      BIWEEKLY = "biweekly",
      MONTHLY = "monthly"
    ].freeze

    NUMBER_OF_PAYMENTS = {
      WEEKLY => 52,
      BIWEEKLY => 26,
      MONTHLY => 12
    }.freeze

    def self.number_of_payments(frequency)
      NUMBER_OF_PAYMENTS[frequency]
    end

    def self.payments_over_period(frequency, amortization_period_years)
      number_of_payments(frequency) * amortization_period_years
    end
  end
end
