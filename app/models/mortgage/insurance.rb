module Mortgage
  class Insurance
    MAX_AMOUNT = 1_000_000
    include ActiveModel::Validations

    attr_accessor :loan_amount, :down_payment_percent

    validates :loan_amount, numericality: { greater_than: 0 }
    validates :down_payment_percent, numericality: { greater_than_or_equal_to: 5, less_than: 100 }

    def initialize(loan_amount, down_payment_percent)
      @loan_amount = loan_amount
      @down_payment_percent = down_payment_percent
    end

    def cost
      raise errors.full_messages.join("\n") unless valid?

      return 0 if @loan_amount > MAX_AMOUNT

      (insurance_rate * @loan_amount).round(2)
    end

    # Down payment     Insurance Cost
    # 5-9.99%        		3.15%
    # 10-14.99%    		  2.4%
    # 15%-19.99%  		  1.8%
    # 20%+         		  N/A
    def insurance_rate
      case @down_payment_percent
      when 5...10.0
        BigDecimal("0.0315")
      when 10...15.0
        BigDecimal("0.024")
      when 15...20.0
        BigDecimal("0.018")
      else
        0
      end
    end
  end
end
