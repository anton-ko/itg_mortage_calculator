module Mortgage
  class MaxMortgageQuote
    include ActiveModel::Model

    attr_accessor :payment_amount, :payment_schedule, :amortization_period

    validates :payment_amount, numericality: { greater_than: 0 }
    validates :payment_schedule, inclusion:
      { in: PaymentSchedule::FREQUENCIES,
        message: "can only be #{Mortgage::PaymentSchedule::FREQUENCIES.to_sentence(last_word_connector: ' or ')}" }
    validates :amortization_period, numericality:
      { greater_than_or_equal_to: AmortizationPeriod::MIN_YEARS,
        less_than_or_equal_to: AmortizationPeriod::MAX_YEARS,
        only_integer: true }

    def principal
      raise errors.full_messages.join("\n") unless valid?

      calculate_principal
    end

    private

    # See the link for explanation of the math
    # https://www.wikihow.com/Calculate-Mortgage-Payments#Calculating-Mortgage-Payments-with-an-Equation
    def calculate_principal
      rate_per_period = InterestRate.current_value / PaymentSchedule.number_of_payments(@payment_schedule)
      payment_number = Mortgage::PaymentSchedule.payments_over_period(@payment_schedule, @amortization_period.to_i)
      k = (1 + rate_per_period)**payment_number

      ((BigDecimal(payment_amount) * (k - 1)) / (rate_per_period * k)).truncate(2)
    end
  end
end
