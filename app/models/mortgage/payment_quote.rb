module Mortgage
  class PaymentQuote
    include ActiveModel::Model

    attr_accessor :asking_price, :down_payment, :amortization_period, :payment_schedule

    validates :asking_price, numericality: { greater_than: 0 }
    validates :down_payment, numericality: { greater_than_or_equal_to: :min_down_payment, less_than: :asking_price }
    validates :payment_schedule, inclusion:
      { in: PaymentSchedule::FREQUENCIES,
        message: "can only be #{PaymentSchedule::FREQUENCIES.to_sentence(last_word_connector: ' or ')}" }
    validates :amortization_period, numericality:
      { greater_than_or_equal_to: AmortizationPeriod::MIN_YEARS,
        less_than_or_equal_to: AmortizationPeriod::MAX_YEARS,
        only_integer: true }

    def down_payment_percent
      (BigDecimal(down_payment) / BigDecimal(asking_price)) * BigDecimal(100)
    end

    def loan_amount
      BigDecimal(asking_price) - BigDecimal(down_payment)
    end

    def min_down_payment
      Mortgage::MinDownPayment.calculate(asking_price)
    end

    def total_loan_amount
      return nil unless valid?

      loan_amount = BigDecimal(asking_price) - BigDecimal(down_payment)
      loan_amount + Insurance.new(loan_amount, down_payment_percent).cost
    end

    def payment_amount
      rate_per_period = InterestRate.current_value / PaymentSchedule.number_of_payments(@payment_schedule)
      payment_number = PaymentSchedule.payments_over_period(@payment_schedule, @amortization_period.to_i)
      k = (1 + rate_per_period)**payment_number

      ((total_loan_amount * (rate_per_period * k)) / (k - 1)).round(2)
    end
  end
end
