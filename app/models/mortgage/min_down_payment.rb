module Mortgage
  module MinDownPayment
    # * Must be at least 5% of first $500k plus 10% of any amount above $500k (So $50k on a $750k mortgage)

    LOW_RATE = BigDecimal("0.05")
    LOW_RATE_THRESHOLD = 500_000
    HIGHER_RATE = BigDecimal("0.1")

    def self.calculate(asking_price)
      ([BigDecimal(asking_price), LOW_RATE_THRESHOLD].min * LOW_RATE) + # low rate below threshold
        ([BigDecimal(asking_price) - LOW_RATE_THRESHOLD, 0].max * HIGHER_RATE) # higher rate for amount above threshold
    end
  end
end
