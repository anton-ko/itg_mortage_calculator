module Mortgage
  module AmortizationPeriod
    MIN_YEARS = 5
    MAX_YEARS = 25
  end
end
