# Stores mortgage interest rate value
#
# Call InterestRate.current_value to get current rate

class InterestRate < AppSetting
  attribute :value, :decimal, default: -> { 0.025 }
  alias_attribute :rate, :value

  validates :rate, numericality: { greater_than: 0, less_than: 1 }

  class << self
    def default_scope
      where(key: name.underscore)
    end

    alias current take

    def current_value
      current&.rate
    end
  end
end
