class AppSetting < ApplicationRecord
  validates :key, uniqueness: true
end
