FactoryBot.define do
  factory :interest_rate do
    rate { 0.025 }
  end
end
