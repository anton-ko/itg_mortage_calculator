# README

## How to run this

The app relies on SQLite DB to store current interest rate.

Run db:setup to prepare the database:
```
bundle exec rails db:setup
```

Then the app can be run with `rails s`
```
bundle exec rails s
```

## API

### GET `/payment-amount`

Get the recurring payment amount of a mortgage taking insurance in the account

#### Params

`asking_price` Asking price

`down_payment` Downpayment amount

`payment_schedule` One of the following strings: `monthly`, `biweekly`, `weekly`

`amortization_period` Amortization period length in years

#### Response

Payment amount per scheduled payment in JSON format:

```
{ "payment_amount":"193.78" }
```
If input is invalid, response will have 400 status code.
The body will contain validation errors:

```
{"errors":["Payment schedule can only be weekly, biweekly or monthly"]}
```

### GET `/mortgage-amount`

Get the maximum mortgage amount (principal)

#### Params

`payment_amount` Monthly payment amount

`payment_schedule` One of the following strings: `monthly`, `biweekly`, `weekly`

`amortization_period` Amortization period length in years

#### Response

Endpoint responds with JSON containing the maximum mortgage principal:
```
{"maximum_mortgage_amount":"1000.0"}
```

If input is invalid, response will have 400 status code.
Validation errors are returned in the body:
 
```
{"errors":["Payment must be greater than 0"]
``` 

#### Example

```
curl "http://localhost:3000/mortgage-amount?payment_amount=1000&payment_schedule=weekly&amortization_period=25"
curl "http://localhost:3000/mortgage-amount" # see validation errors
```

### PATCH `/interest-rate`

Change the interest rate used by the application.

####  Params

`rate` - string representation of decimal rate e.g. '0.015' for 1.5% 

#### Response

Response is a JSON indicating the old and new interest rate:
```
{"previous_rate":"0.025","current_rate":"0.015"}
```

If input is invalid, response will have 400 status code.
Validation errors are returned the body:
 
```
{"errors":["Rate must be greater than 0"]
``` 
#### Example

```
curl --data '{"rate": "0.015"}'  -XPATCH http://localhost:3000/interest-rate -H "content-type: application/json"
```

