Rails.application.routes.draw do
  get "payment-amount" => "payment_amount#show"
  get "mortgage-amount" => "max_mortgage_amount#show"
  patch "interest-rate" => "interest_rate#update"
end
